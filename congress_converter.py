import csv

reader = csv.reader(open("./congress_network/congress.edgelist"), delimiter=' ')


with open('congress_network.csv', 'w', newline='') as csvfile:
    fieldnames = ['Source', 'Target', 'Weight']
    writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
    writer.writerow({"Source": fieldnames[0], "Target": fieldnames[1], 'Weight': fieldnames[2]})

    for line in reader:
        print(line)
        writer.writerow({"Source": line[0], "Target": line[1], 'Weight': line[3][:-1]})

