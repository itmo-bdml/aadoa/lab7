## Lab 7

### Algorithms on graphs. Tools for network analysis

---
[Report (google doc)](https://docs.google.com/document/d/1JXHjeGxdPYfnOxUS-RLQzRmFYI7kZr14QpXB2lo6SXA)

Performed by:
* Elya Avdeeva
* Gleb Mikloshevich

[dataset](https://snap.stanford.edu/data/congress-twitter.html)